<?php

$conn = new mysqli('localhost', 'root', 'secret', 'test');

$result = $conn->query("SELECT * FROM quest");

if (!$result) {
    $obj = Array(
        'state' => 'error',
        'data' => null
    );
    print json_encode($obj);
    $conn->close();
    exit();
}
$quest = array();
while ($row = $result->fetch_assoc()){
    $quest[] = $row;
}
$obj = Array(
    'state' => 'success',
    'data' => $quest
);

print json_encode($obj);

?>