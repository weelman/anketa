<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Тест</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">

</head>

<body>

    <div class="container" id="fullname">
        <div class="col-sm-3">
            <input type="text" class="form-control" id="name1" placeholder="Имя" pattern="[А-Яа-я]" data-toggle="tooltip" data-placement="bottom" onkeypress="valid1()" title="Для ввода доступны только буквы кириллического алфавита">
        </div>
        <div class="col-sm-3">
            <input type="text" class="form-control" id="name2" placeholder="Фамилия" pattern="[А-Яа-я]" data-toggle="tooltip" data-placement="bottom" onkeypress="valid2()" title="Для ввода доступны только буквы кириллического алфавита">
        </div>

        <button type="button" onclick="subm()" class="btn btn-default">Отправить</button>
    </div>
    <br />
    <div class="container col-sm-offset-2 col-sm-8" id="test">
        <div class="well well-lg">
            <H4>№1</H4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, aliquam, impedit nostrum commodi assumenda ab error, suscipit, minima laboriosam unde laudantium quae debitis. Beatae expedita, repudiandae consequuntur assumenda amet at?</p>
            <input type="text" class="form-control">
        </div>

    </div>
    <script src="js/print.js"></script>
    <script src="js/valid.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>